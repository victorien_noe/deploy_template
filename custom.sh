echo "enter hostname"
read host
echo "enter ip"
read ip

hostname $host
sed -i "s/template/$host/" /etc/hostname
sed -i "s/template/$host/" /etc/hosts
sed -i "s/192.168.62.50/$ip/" /etc/netplan/50-cloud-init.yaml

echo "" >> /etc/fstab
echo "192.168.62.12:/media	/mnt/media	nfs4	auto	0	0" >> /etc/fstab
echo "192.168.62.12:/config	/mnt/config	nfs4	auto	0	0" >> /etc/fstab
echo "192.168.62.12:/cloud	/mnt/cloud	nfs4	auto	0	0" >> /etc/fstab

